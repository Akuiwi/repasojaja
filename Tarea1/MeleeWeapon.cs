﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    abstract class MeleeWeapon : Weapon
    {
        protected float damagePerSeconds;

        public MeleeWeapon(string name,float damage,float velocityAttack,float dps) : base(name,damage,velocityAttack)
        {
            this.damagePerSeconds = dps;
        }

        
        public override void Datos()
        {
            
        }

        public override string GetName()
        {
            return name;
        }

        public override float GetDamage()
        {
            return damage;
        }

    }
}
