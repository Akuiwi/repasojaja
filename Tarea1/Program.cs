﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("if-else");
            Console.WriteLine("escribe un numero para verifiar si es mayor a 5");
            int num = Convert.ToInt32(Console.ReadLine());

            
            if(num == 5)
            {
                Console.WriteLine("el numero es 5");
            }
            else if (num > 5)
            {
                Console.WriteLine("el numero es mayor a 5");
            }
            else
            {
                Console.WriteLine("el numero es menor a 5");
            }

            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("for - arreglo");

            int[] numArray = new int[5];
            Random random = new Random();

            for (int i = 0; i < numArray.Length; i++)
            {
                
                numArray[i] = random.Next(0,30);
                Console.WriteLine("el numero en el espacio " + i + " es " + numArray[i]);
            }

            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("metodo - parametro (sumar dos numeros)");

            Metodos sumar = new Metodos();

            Console.WriteLine("escribe un numero para sumarlo");
            int numA = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("con que numero lo quieres sumar");
            int numB = Convert.ToInt32(Console.ReadLine());

            int a = sumar.Suma(numA, numB);

            Console.WriteLine(a);

            Console.ReadLine();
            Console.Clear();


            Weapon gun = new Gun(3, "Pistola", 20, 0.5f);
            Weapon rifle = new Rifle(8, "Rifle", 50, 0.2f);
            Weapon espada = new Sword("espada", 50, 0.5f, 0);

            List<Weapon> weapons = new List<Weapon>();

            Dictionary<string, float> detalles = new Dictionary<string, float>();
            bool continuar = true;
            detalles[gun.GetName()] = gun.GetDamage();
            detalles[rifle.GetName()] = rifle.GetDamage();
            detalles[espada.GetName()] = espada.GetDamage();

            while (continuar)
            {
                Console.WriteLine("---------comprar arma---------");
                Console.WriteLine("1. Pistola");
                Console.WriteLine("2. Rifle");
                Console.WriteLine("3. Espada");
                Console.WriteLine("4. Inventario");
                Console.WriteLine("5. Ver el daño de las armas");
                Console.WriteLine("6. Salir");
                string selection = Console.ReadLine();

                
                Console.Clear();

                switch (selection)
                {
                    case "1":
                        Console.WriteLine("pistola comprada");
                        Console.WriteLine("enter para continuar");
                        Console.ReadLine();
                        Console.Clear();
                        weapons.Add(gun) ;break;
                        
                    case "2":
                        Console.WriteLine("rifle comprada");
                        Console.WriteLine("enter para continuar");
                        Console.ReadLine();
                        Console.Clear();
                        weapons.Add(rifle); break;
                    case "3":
                        Console.WriteLine("espada comprada");
                        Console.WriteLine("enter para continuar");
                        Console.ReadLine();
                        Console.Clear();
                        weapons.Add(espada); break;
                    case "4": 
                        for(int i = 0; i < weapons.Count();i++)
                        {
                            Console.WriteLine(weapons[i].GetName());
                        }
                        Console.WriteLine("enter para continuar");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "5":
                        foreach (var kvp in detalles)
                        {
                            Console.WriteLine($"Nombre: {kvp.Key}, daño: {kvp.Value}");
                        }
                        Console.WriteLine("enter para continuar");
                        Console.ReadLine();
                        Console.Clear();

                        break;

                    case "6": continuar = false; break;
                }
            }
        }
    }
}
