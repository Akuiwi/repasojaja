﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    abstract class DistanceWeapon : Weapon
    {
        protected float rangeAttack;

       public  DistanceWeapon(float rangeAttack,string name,float damage,float velocityAttack) : base(name,damage, velocityAttack)
       {
           this.rangeAttack = rangeAttack;
       }

        public override void Datos()
        {

        }

        public override string GetName()
        {
            return name;
        }

        public override float GetDamage()
        {
            return damage;
        }

    }
}
