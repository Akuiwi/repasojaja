﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    class Gun : DistanceWeapon
    {
        public Gun(float rangeAttack,string name,float damage,float velocityAttack) : base(rangeAttack,name,damage, velocityAttack)
        {

        }

        public override void Datos()
        {
            Console.WriteLine(name);
            Console.WriteLine("rango de ataque : " + rangeAttack);
            Console.WriteLine("velocidad de ataque : " + damage);
            Console.WriteLine("enfriamiento de ataque : " + velocityAttack);
        }

        public string GetName()
        {
            return name;
        }
    }
}
