﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    abstract class Weapon
    {
        protected string name;
        protected float damage;
        protected float velocityAttack; 

        public Weapon(string name,float damage,float velocityAttack)
        {
            this.name = name;
            this.damage = damage;
            this.velocityAttack = velocityAttack;
        }

        public abstract void Datos();

        public abstract string GetName();
        public abstract float GetDamage();

    }
}
