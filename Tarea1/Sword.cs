﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea1
{
    class Sword : MeleeWeapon, IDPS
    {
        public Sword(string name,float damage,float velocityAttack,float dps) : base(name,damage,velocityAttack,dps)
        {

        }

        public float CalculateDamagePerSecond()
        {

            return damage / velocityAttack;
        }

        public override void Datos()
        {
            damagePerSeconds = CalculateDamagePerSecond();
            Console.WriteLine(name);
            Console.WriteLine("daño por segundo : " + damagePerSeconds);
            Console.WriteLine("velocidad de ataque : " + damage);
            Console.WriteLine("enfriamiento de ataque : " + velocityAttack);
        }

        

    }
}
